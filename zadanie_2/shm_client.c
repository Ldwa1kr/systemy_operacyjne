#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <stdbool.h>
#include "common.h"

key_t get_shm_key();

int shm_get();

void register_pid();

void deregister_pid();

void print_pids();

bool contains(__pid_t * pid, __pid_t *pids, int capacity);

int main(int argc, char *argv[]) {
    char command[1];

    while(1)
    {
        printf("1: register pid, 2: deregister pid , 3: print pids, 4: exit \n");
        fscanf (stdin, "%s", command);
        if(*command == '1'){
            register_pid();
        } else if(*command == '2'){
            deregister_pid();
        } else if(*command == '3'){
            print_pids();
        } else if(*command == '4'){
            exit(0);
        }
    }
}

void print_pids() {
    int shm_id = shm_get();
    if(shm_id==-1){
        perror("shared memory not created \n");
        return;
    }
    pid_mem *pids = shmat(shm_id, 0, SHM_RDONLY);
    for(int i =0; i< pids->registered_count; i++){
        printf("pid: %ld \n", (long) pids->pids[i]);
    }
    shmdt(pids);
}

void deregister_pid() {
    int shm_id = shm_get();
    if(shm_id==-1){
        perror("shared memory not created \n");
        return;
    }
    pid_mem *pids = shmat(shm_id,0,0);
    pthread_mutex_lock(&pids->mutex);
    __pid_t current_pid = getpid();
    for(int i=0; i<pids->registered_count; i++) {
        if(current_pid == pids->pids[i]){
            pids->pids[i] = pids->pids[--pids->registered_count];
            break;
        }
    }
    pthread_mutex_unlock(&pids->mutex);
    shmdt(pids);
}

void register_pid() {
    int shm_id = shm_get();
    if(shm_id==-1){
        perror("shared memory not created \n");
        return;
    }
    pid_mem *pids = shmat(shm_id,0,0);
    pthread_mutex_lock(&pids->mutex);
    __pid_t current_pid = getpid();
    if(!contains(&current_pid, pids->pids, pids->registered_count)){
        if(pids->registered_count!=SHM_PIDS_SIZE){
            pids->pids[pids->registered_count++] = current_pid;
        } else {
            printf("maximum capacity reached \n");
        }
    } else {
        printf("already registered \n");
    }
    pthread_mutex_unlock(&pids->mutex);
    shmdt(pids);
}

bool contains(__pid_t * pid, __pid_t *pids, int capacity) {
    for(int i =0; i<capacity; i++){
        if(pids[i] == *pid){
            return true;
        }
    }
    return false;
}

int shm_get() {
    return shmget(get_shm_key(), SEGMENT_SIZE, 0);
}

key_t get_shm_key() {
    return ftok(SHM_PATHNAME, PROJECT_ID);
}
