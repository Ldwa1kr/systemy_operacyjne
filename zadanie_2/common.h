#include <unistd.h>


#define SEGMENT_SIZE 65
#define PROJECT_ID 66
#define SHM_PIDS_SIZE 5
#define SHM_PATHNAME "."

typedef struct {
    pthread_mutex_t mutex;
    int registered_count;
    __pid_t pids[5];
} pid_mem;
