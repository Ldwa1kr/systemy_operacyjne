#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include "common.h"

void create_shm();

void delete_shm();

key_t get_shm_key();

int shm_get(int i);

int main(int argc, char *argv[]) {
    char command[1];
    while(1)
    {
        printf("1: create shared memory, 2: delete shared memory , 3: exit \n");
        fscanf (stdin, "%s", command);
        if(*command == '1'){
            create_shm();
        } else if(*command == '2'){
            delete_shm();
        } else if(*command == '3'){
            delete_shm();
            exit(0);
        }
    }
}

void delete_shm() {
    int shm_id = shm_get(0);
    if(shm_id<0){
        perror("cannot delete \n");
        return;
    }
    struct shmid_ds shm_info;
    shmctl(shm_id, IPC_STAT, &shm_info);
    if(shm_info.shm_nattch){
        printf("cannot remove shared memory: non-zero attaches \n");
        return;
    }
    shmctl(shm_id, IPC_RMID, 0);
}

void create_shm() {
    int shm_id = shm_get(0);
    if(shm_id>0){
        printf("shared memory already exists \n");
        return;
    }

    shm_id = shm_get(IPC_EXCL | IPC_CREAT | 0666);
    if(shm_id==-1){
        perror("cannot create \n");
        return;
    }
    pid_mem * pid_shm = shmat(shm_id,0,0);
    pid_shm->registered_count=0;
    pthread_mutex_init(&pid_shm->mutex, NULL);
    shmdt(pid_shm);
}

int shm_get(int options) {
    return shmget(get_shm_key(), SEGMENT_SIZE, options);
}

key_t get_shm_key() {
    return ftok(SHM_PATHNAME, PROJECT_ID);
}
