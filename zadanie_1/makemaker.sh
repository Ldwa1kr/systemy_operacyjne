#!/usr/bin/env bash

catalog=$1

if [[ -z $catalog ]]; then
    echo "no catalog given. usage: script [catalog]"
    exit 1
fi

cd $catalog
main_file=""
source_files=`ls ./*.c `
for file in $source_files
do
   if [[ -n `grep 'int main(' $file` ]]; then
        if [[ -n $main_file ]]; then
            echo "main function found in multiple files"
            exit 1
        fi
        main_file=$file
   fi
done

if [[ -z $main_file ]]; then
    echo "no file with main function"
    exit 1
fi
program_name=`echo $main_file | sed 's|\.c||' | sed 's|\./||'`
object_files=`echo $source_files | sed 's|\.c|.o|g'`

rm -f Makefile

echo "
CC = gcc
$program_name: $object_files
	\$(CC) $object_files -o $program_name
" >> Makefile

for file in $source_files
do
   header_files=`grep '#include ' $file | sed "s|#include |headers/|g" | sed 's|"||g'`
   object_file=`echo $file | sed 's|.c|.o|g' | sed 's|./||'`
echo "
$object_file: $file `echo $header_files`
	\$(CC) -Iheaders $file -c -o $object_file
" >> Makefile
done
